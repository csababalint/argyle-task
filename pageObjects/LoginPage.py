from utilities.customLogger import LogGen
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class LoginPage:
    input_email_id = "sign-in-email-input"
    input_pass_id = "sign-in-password-input"
    btn_login_xpath = "//button[@type='submit']"
    invalid_login_input_xpath = (
        "//p[contains(text(), 'Invalid email and password combination')]"
    )
    argyle_logo_xpath = (
        "//div[@id='auth-wrapper-form-container']/div[1]//*[name()='svg']"
    )
    btn_signUp_xpath = "//a[normalize-space()='Join Argyle']"
    signUp_page_title_xpath = "//h1[@data-hook='sign-up-page-title']"
    signUp_first_name_xpath = "//input[@id='sign-up-first-name-input']"
    signUp_last_name_xpath = "//input[@id='sign-up-last-name-input']"
    signUp_email_xpath = "//input[@id='sign-up-work-email-input']"
    signUp_phone_xpath = "//input[@id='sign-up-phone-input']"
    signUp_service_agreement_xpath = (
        "//div[@id='auth-wrapper-form-container']//div//div[4]"
    )
    signUp_submit_btn_xpath = (
        "//div[contains(text(),'Join Argyle')]//parent::div//parent::button"
    )
    signUp_login_btn_xpath = "//a[normalize-space()='Log in']"
    signUp_page_footer_xpath = (
        "//body/div[@id='root']/div/div/div[@id='auth-wrapper-form-container']/div[4]"
    )
    login_page_title_xpath = "//h1[normalize-space()='Sign in to Argyle']"
    login_page_img_xpath = "//body//div[@id='root']//div//div//div[1]//img[1]"
    login_page_learn_more_xpath = "//a[normalize-space()='Learn more']"
    login_page_terms_xpath = "//div[@data-hook='page-wrapper-privacy-link']"

    logger = LogGen.loggen()

    def __init__(self, driver):
        self.driver = driver

    def setUsername(self, username):
        self.driver.find_element_by_id(self.input_email_id).clear()
        self.driver.find_element_by_id(self.input_email_id).send_keys(username)

    def setPassword(self, password):
        self.driver.find_element_by_id(self.input_pass_id).clear()
        self.driver.find_element_by_id(self.input_pass_id).send_keys(password)

    def clickLogin(self):
        self.driver.find_element_by_xpath(self.btn_login_xpath).click()

    def invalidLogin(self):
        try:
            element = WebDriverWait(self.driver, 30).until(
                EC.presence_of_element_located(
                    ("xpath", self.invalid_login_input_xpath)
                )
            )
            self.logger.info(
                "****************** Invalid Login indicator displayed ******************"
            )
            assert True
        except:
            self.driver.save_screenshot(".\\Screenshots\\" + "test_invalidLogin.png")
            self.logger.error("****************** Fail ******************")
            assert False

    def click_ArgyleLogo(self):
        argyle_logo = self.driver.find_element_by_xpath(self.argyle_logo_xpath)
        argyle_logo.click()

        act_url = self.driver.current_url

        if act_url == "https://argyle.com/":
            self.logger.info(
                "****************** Home Page Loaded Successfully ******************"
            )
            assert True
        else:
            self.driver.save_screenshot(".\\Screenshots\\" + "test_ArgyleLogo.png")
            self.logger.error(
                "****************** Home Page NOT Loaded <Fail> ******************"
            )
            assert False

    def signUpPage(self):
        self.logger.info("****************** Verifying SignUp Page ******************")
        btn_signUp = self.driver.find_element_by_xpath(self.btn_signUp_xpath)
        btn_signUp.click()
        try:
            for page_element in (
                self.signUp_page_title_xpath,
                self.argyle_logo_xpath,
                self.signUp_first_name_xpath,
                self.signUp_last_name_xpath,
                self.signUp_email_xpath,
                self.signUp_phone_xpath,
                self.signUp_service_agreement_xpath,
                self.signUp_submit_btn_xpath,
                self.signUp_login_btn_xpath,
                self.signUp_page_footer_xpath,
            ):

                element = WebDriverWait(self.driver, 10).until(
                    EC.presence_of_element_located(("xpath", page_element))
                )
            assert True
        except:
            self.driver.save_screenshot(".\\Screenshots\\" + "test_signUp_page.png")
            self.logger.error(
                "****************** Sign Up Page Elements NOT Loaded <Fail> ******************"
            )
            assert False

    def loginPage_elements(self):
        self.logger.info(
            "****************** Verifying Login Page Elements ******************"
        )
        try:
            for page_element in (
                self.login_page_title_xpath,
                self.argyle_logo_xpath,
                self.btn_login_xpath,
                self.btn_signUp_xpath,
                self.login_page_img_xpath,
                self.login_page_learn_more_xpath,
                self.login_page_terms_xpath,
            ):
                element = WebDriverWait(self.driver, 10).until(
                    EC.presence_of_element_located(("xpath", page_element))
                )
                self.logger.info(
                    "****************** Login Page Elements Loaded Successfully ******************"
                )
            assert True
        except:
            self.driver.save_screenshot(
                ".\\Screenshots\\" + "test_loginPage_elements.png"
            )
            self.logger.error(
                "****************** Login Page Elements NOT Loaded <Fail> ******************"
            )
            assert False

    def validLogin(self):
        act_url = self.driver.current_url

        if act_url != "https://console.argyle.com/sign-in":
            self.logger.info(
                "****************** Logged In Successfully ******************"
            )
            assert True
        else:
            self.driver.save_screenshot(".\\Screenshots\\" + "test_validLogIn.png")
            self.logger.error(
                "****************** Home Page NOT Loaded Successfully <Fail> ******************"
            )
            assert False
