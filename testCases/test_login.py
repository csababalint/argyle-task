from pageObjects.LoginPage import LoginPage
from utilities.readProperties import ReadConfig
from utilities.customLogger import LogGen


class Test_Login_Page:
    baseURL = ReadConfig.getAppURL()
    dummy_username = ReadConfig.getDummyUsername()
    dummy_password = ReadConfig.getDummyPassword()
    valid_username = ReadConfig.getValidUsername()
    valid_password = ReadConfig.getValidPassword()
    logger = LogGen.loggen()

    def test_invalid_login_001(self, setup):
        self.logger.info("****************** Test_Invalid_Login_001 ******************")
        self.logger.info(
            "****************** Verifying Invalid Login ******************"
        )
        self.driver = setup
        self.driver.get(self.baseURL)
        self.lp = LoginPage(self.driver)
        self.lp.setUsername(self.dummy_username)
        self.lp.setPassword(self.dummy_password)
        self.lp.clickLogin()
        self.lp.invalidLogin()
        self.driver.close()

    def test_valid_login_002(self, setup):
        self.logger.info("****************** Test_Valid_Login_002 ******************")
        self.logger.info("****************** Verifying Valid Login ******************")
        self.driver = setup
        self.driver.get(self.baseURL)
        self.lp = LoginPage(self.driver)
        self.lp.setUsername(self.valid_username)
        self.lp.setPassword(self.valid_password)
        self.lp.clickLogin()
        self.lp.validLogin()
        self.driver.close()

    def test_argyleLogo_003(self, setup):
        self.logger.info("****************** Test_Argyle_Logo_003 ******************")
        self.logger.info(
            "****************** Verifying The Argyle Logo ******************"
        )
        self.driver = setup
        self.driver.get(self.baseURL)
        self.lp = LoginPage(self.driver)
        self.lp.click_ArgyleLogo()
        self.driver.close()

    def test_signUp_004(self, setup):
        self.logger.info("****************** Test_Sign_Up_004 ******************")
        self.logger.info(
            "****************** Verifying Sign Up Functionality ******************"
        )
        self.driver = setup
        self.driver.get(self.baseURL)
        self.lp = LoginPage(self.driver)
        self.lp.signUpPage()
        self.driver.close()

    def test_login_page_elements_005(self, setup):
        self.logger.info("****************** Test_Sign_Up_005 ******************")
        self.logger.info(
            "****************** Verifying Login Functionality ******************"
        )
        self.driver = setup
        self.driver.get(self.baseURL)
        self.lp = LoginPage(self.driver)
        self.lp.loginPage_elements()
        self.driver.close()
