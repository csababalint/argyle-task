import configparser

config = configparser.RawConfigParser()
config.read(".\\Configurations\\config.ini")


class ReadConfig:
    @staticmethod
    def getAppURL():
        url = config.get("common variables", "baseURL")
        return url

    @staticmethod
    def getDummyUsername():
        dummy_username = config.get("common variables", "dummy_username")
        return dummy_username

    @staticmethod
    def getDummyPassword():
        dummy_password = config.get("common variables", "dummy_password")
        return dummy_password

    def getValidUsername():
        valid_username = config.get("common variables", "valid_username")
        return valid_username

    @staticmethod
    def getValidPassword():
        valid_password = config.get("common variables", "valid_password")
        return valid_password
